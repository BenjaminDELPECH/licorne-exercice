import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UnicornListComponent} from "./modules/unicorns/unicorn-list/unicorn-list.component";

const routes: Routes = [
  {
    path: '**',
    component: UnicornListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
