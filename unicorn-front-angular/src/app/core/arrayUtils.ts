export function removeElemFromArray<T>(array:T[], elem:T) {
  array.splice(
    array.indexOf(elem), 1
  );
  return array;
}

export function addItemToStart<T>(array:T[], item:T){
  array.unshift(item);
  return array;
}
