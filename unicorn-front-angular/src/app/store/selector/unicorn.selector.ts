import {Unicorn} from "../../modules/unicorns/models/unicorn";
import {createFeatureSelector, createSelector} from "@ngrx/store";
import {unicornFeatureKey} from "../reducers/unicorn.reducer";

export interface UnicornState {
  unicornList : Unicorn[],
  breedingCandidateList: number[]
}

export const getUnicornState = createFeatureSelector<UnicornState>(unicornFeatureKey);

export const getUnicorns = createSelector(
  getUnicornState,
  state => state.unicornList
);

export const getBreedingCandidateList = createSelector(
  getUnicornState,
  state=>state.breedingCandidateList
)

