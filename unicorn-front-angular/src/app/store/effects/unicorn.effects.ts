import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {addUnicorn, loadUnicorns, loadUnicornsSuccess} from "../actions/unicorn.actions";
import {map} from 'rxjs/operators';
import {Unicorn} from "../../modules/unicorns/models/unicorn";
import {unicornFeatureKey} from "../reducers/unicorn.reducer";
import {LocalStorageService} from "../../api/localStorage.service";


@Injectable()
export class UnicornEffects{
  constructor(private actions$: Actions, private localStorageService: LocalStorageService) {
  }

  setUnicorn$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(addUnicorn),
        map(action => {
          let unicornList: Unicorn[] = this.localStorageService.getItem(unicornFeatureKey);
            if(!unicornList){
              unicornList = []
            }
            unicornList.push(action.unicorn)
            this.localStorageService.setItem(unicornFeatureKey, unicornList);
          })
      ),
    { dispatch: false }
  );

  loadUnicorns$= createEffect(() =>
    this.actions$.pipe(
      ofType(loadUnicorns),
      map((): Unicorn[] => this.localStorageService.getItem(unicornFeatureKey)),
      map((unicorns) => {
        if(!unicorns){
          unicorns = [];
        }
        return loadUnicornsSuccess({ unicorns });
      }
      )
    )
  );

}
