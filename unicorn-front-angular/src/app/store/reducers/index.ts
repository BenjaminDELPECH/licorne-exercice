import {ActionReducerMap, MetaReducer} from "@ngrx/store";
import * as unicorn from './unicorn.reducer';
import {UnicornState} from "../selector/unicorn.selector";

export interface AppState {
  [unicorn.unicornFeatureKey] : UnicornState
}

export const reducers : ActionReducerMap<AppState> ={
  [unicorn.unicornFeatureKey] : unicorn.reducer
}

export const metaReducers : MetaReducer<AppState>[] = [];
