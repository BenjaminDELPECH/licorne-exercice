import {Action, createReducer, on} from "@ngrx/store";
import {
  addToBreedingCandidateList,
  addUnicorn,
  loadUnicorns,
  loadUnicornsSuccess,
  removeFromBreedingCandidateList
} from "../actions/unicorn.actions";
import {UnicornState} from "../selector/unicorn.selector";
import {addItemToStart, removeElemFromArray} from "../../core/arrayUtils";

export const unicornFeatureKey = 'unicornList'


export const initialState: UnicornState = {
  unicornList: [],
  breedingCandidateList: []
}



const unicornReducer = createReducer(
  initialState,
  on(addUnicorn, (state, {unicorn}) => ({...state, unicornList:  addItemToStart([...state.unicornList], unicorn)})),
  on(loadUnicorns, (state) => ({...state})),
  on(loadUnicornsSuccess, (state, {unicorns}) => ({...state, unicornList: state.unicornList.concat(unicorns)})),

  on(addToBreedingCandidateList, (state, {unicornId}) => ({...state, breedingCandidateList: state.breedingCandidateList.concat(unicornId)})),
  on(removeFromBreedingCandidateList, (state, {unicornId}) => ({...state, breedingCandidateList: removeElemFromArray([...state.breedingCandidateList], unicornId)})),
)

export function reducer(state: UnicornState | undefined, action: Action) {
  return unicornReducer(state, action);
}
