import {createAction, props} from '@ngrx/store';
import {Unicorn} from "../../modules/unicorns/models/unicorn";

export enum UnicornActionsType {
  LoadUnicorns = '[Unicorn List] Load Unicorns',
  LoadUnicornSuccess = '[Unicorn List] Load Unicorns Success',
  LoadAttendeesFail = '[Unicorn List] Load Unicorns fail'
}

export const loadUnicorns = createAction('[Unicorn list] load unicorns');
export const loadUnicornsSuccess = createAction('[Unicorn] load unicorn success', props<{unicorns: Unicorn[]}>());
export const loadUnicornsError = createAction('[Unicorn] load unicorn error', props<{payload: any}>())

export const addUnicorn = createAction('[Unicorn] add Unicorn', props<{unicorn: Unicorn}>());
export const storeUnicorn = createAction('[Unicorn] store unicorn', props<{unicorn: Unicorn}>());

export const addToBreedingCandidateList = createAction('[Unicorns] add to breeding list', props<{unicornId:number}>());
export const removeFromBreedingCandidateList = createAction('[Unicorns] remove from breeding list',props<{unicornId:number}>());


