import {Component, Input} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/reducers";
import {addToBreedingCandidateList, removeFromBreedingCandidateList} from "../../../store/actions/unicorn.actions";

@Component({
  selector: 'app-unicorn',
  templateUrl: './unicorn.component.html',
  styleUrls: ['./unicorn.component.scss']
})
export class UnicornComponent  {

  @Input() unicorn: any;

  @Input() index: any;

  checkBoxSelectMateValue: boolean = false

  constructor(private store: Store<AppState>) {
  }


  onSelectMateCheckBoxChange(value: boolean) {
    if(value){
      this.store.dispatch(addToBreedingCandidateList({unicornId:this.unicorn.id}))
    }else{
      this.store.dispatch(removeFromBreedingCandidateList({unicornId:this.unicorn.id}))
    }
  }

}
