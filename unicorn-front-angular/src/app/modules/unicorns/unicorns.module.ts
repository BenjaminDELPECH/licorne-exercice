import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UnicornComponent} from './unicorn/unicorn.component';
import {UnicornListComponent} from './unicorn-list/unicorn-list.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {StoreModule} from "@ngrx/store";
import {metaReducers, reducers} from "../../store/reducers";
import {environment} from "../../../environments/environment";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {EffectsModule} from "@ngrx/effects";
import {UnicornEffects} from "../../store/effects/unicorn.effects";
import { CreateUnicornComponent } from './create-unicorn/create-unicorn.component';


@NgModule({
  declarations: [
    UnicornComponent,
    UnicornListComponent,
    CreateUnicornComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers,{
      metaReducers,
      runtimeChecks:{
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([UnicornEffects]),
  ],
  providers: []
})
export class UnicornsModule {}
