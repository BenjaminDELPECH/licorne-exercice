import { Component } from '@angular/core';
import {FormControl} from "@angular/forms";
import {Gender, Unicorn} from "../models/unicorn";
import {take} from "rxjs/operators";
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store/reducers';
import { getUnicorns } from 'src/app/store/selector/unicorn.selector';
import { addUnicorn, storeUnicorn } from 'src/app/store/actions/unicorn.actions';

@Component({
  selector: 'app-create-unicorn',
  templateUrl: './create-unicorn.component.html',
  styleUrls: ['./create-unicorn.component.scss']
})
export class CreateUnicornComponent {

  unicornList$: Observable<Unicorn[]>;

  id = new FormControl('');
  startColorCss = new FormControl('');
  endColorCss = new FormControl('');
  name = new FormControl("");
  age = new FormControl(20);
  gender = new FormControl(Gender.Male);

  genderEnum:{[index:string]:any}= Gender;

  constructor(private store: Store<AppState>) {
    this.unicornList$ = this.store.pipe(select(getUnicorns));
   }

  ngOnInit(): void {
    //
  }

  handleAdd(): void {
    this.unicornList$.pipe(take(1)).subscribe((unicornList: string | any[])=>{
      const newUnicorn: Unicorn = {
        id: unicornList.length+1,
        name: this.name.value,
        gender: this.gender.value,
        age: this.age.value,
        startColorCss: this.startColorCss.value,
        endColorCss: this.endColorCss.value
      }
      this.addUnicorn(newUnicorn);
    })
  }

  addUnicorn(unicorn: Unicorn): void{
    this.store.dispatch(addUnicorn({unicorn}));
    this.storeUnicorn(unicorn);
  }


  storeUnicorn(unicorn:Unicorn): void{
    this.store.dispatch(storeUnicorn({unicorn:unicorn}))
  }

  genderKeys() : Array<string> {
    return Object.keys(Gender);
  }

}
