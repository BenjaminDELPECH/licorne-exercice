export enum Gender {
  Male='male',
  Female='female'
}

export interface Unicorn{
  id:number,
  name:string,
  age:number,
  gender:Gender,
  startColorCss: string;
  endColorCss: string;
}

