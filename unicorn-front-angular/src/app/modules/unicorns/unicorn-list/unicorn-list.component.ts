import {Component, OnInit} from '@angular/core';
import {Gender, Unicorn} from "../models/unicorn";
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import {AppState} from "../../../store/reducers";
import {addUnicorn, loadUnicorns} from "../../../store/actions/unicorn.actions";
import {getBreedingCandidateList, getUnicorns} from "../../../store/selector/unicorn.selector";
import {take} from "rxjs/operators";

@Component({
  selector: 'app-unicorn-list',
  templateUrl: './unicorn-list.component.html',
  styleUrls: ['./unicorn-list.component.scss'],
  providers: []
})
export class UnicornListComponent implements OnInit {

  unicornList$: Observable<Unicorn[]>;
  breedingCandidateList$ : Observable<number[]>;
  isTwoBreedingCandidate: boolean = false;
  isTwoDifferentSex: boolean = false;


  constructor(private store: Store<AppState>) {
    this.unicornList$ = this.store.pipe(select(getUnicorns));
    this.breedingCandidateList$ = this.store.pipe(select(getBreedingCandidateList))
  }

  ngOnInit(): void {
    this.unicornList$ = this.store.pipe(select(getUnicorns));
    this.store.dispatch(loadUnicorns());
   this.listenToBreedingCandidateChanges();
  }

  reproduction(){
    this.breedingCandidateList$.pipe(take(1)).subscribe(breedingList=>{
      this.unicornList$.pipe(take(1)).subscribe(unicornList=>{

        const unicornBreedingCandidates = unicornList.filter(e=> breedingList.indexOf(e.id)!==-1);

        const maleUnicorn = unicornBreedingCandidates.find(e=>e.gender===Gender.Male)
        const femaleUnicorn = unicornBreedingCandidates.find(e=>e.gender===Gender.Female)
        if(!maleUnicorn || !femaleUnicorn){
          return;
        }
        const babyUnicorn = this.buildBabyUnicorn(maleUnicorn, femaleUnicorn, unicornList);

        this.store.dispatch(addUnicorn({unicorn:babyUnicorn}))
      });
    });
  }

  listenToBreedingCandidateChanges(): any{
    this.breedingCandidateList$.subscribe(breedingList=>{
      this.unicornList$.pipe(take(1)).subscribe(unicornList=>{
        const unicornBreedingCandidates = unicornList.filter(e=> breedingList.indexOf(e.id)!==-1);
        this.isTwoBreedingCandidate  = unicornBreedingCandidates.length === 2;
        this.isTwoDifferentSex =this.isTwoBreedingCandidate &&  [...new Set(unicornBreedingCandidates.map(e=>e.gender))].length == 2;

      })
    })
  }

  buildBabyUnicorn(maleUnicorn:Unicorn, femaleUnicorn:Unicorn, unicornList:Unicorn[]):any {
    let childLeftColor: string;
    let childRightColor: string;
    let childGender: Gender;

    let randomColorMaleNumber = Math.random();
    let maleColorGiven: string;
    maleColorGiven = randomColorMaleNumber < 0.5 ? maleUnicorn.startColorCss : maleUnicorn.endColorCss;

    let femaleColorGiven: string;
    femaleColorGiven = randomColorMaleNumber < 0.5 ? femaleUnicorn.startColorCss : femaleUnicorn.endColorCss;

    let randomPositionNumber = Math.random();
    if (randomPositionNumber < 0.5) {
      childLeftColor = maleColorGiven;
      childRightColor = femaleColorGiven;
    } else {
      childLeftColor = femaleColorGiven;
      childRightColor = maleColorGiven;
    }

    let randomGenderNumber = Math.random();
    childGender = randomGenderNumber < 0.5 ? Gender.Male : Gender.Female;

    return {
      id: unicornList.length + 1,
      gender: childGender,
      age: 0,
      name: maleUnicorn.name + femaleUnicorn.name,
      startColorCss: childLeftColor,
      endColorCss: childRightColor
    };
  }
}
